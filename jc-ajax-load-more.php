<?php
/**
 * Plugin Name: Ajax Load More
 * Plugin URI: https://www.infinite-eye.com
 * Description: Display ajax load more plugin
 * Version: 2017.0.0
 * Author: James Collings
 * Author URI: https://www.infinite-eye.com
 *
 * @package JC Ajax Load More
 * @author James Collings <james@infinite-eye.com>
 * @link https://www.infinite-eye.com
 */

class JC_Ajax_Load_More{

	/**
	 * Plugin Directory
	 *
	 * @var string
	 */
	public $plugin_dir = '';

	/**
	 * Plugin Url
	 *
	 * @var string
	 */
	public $plugin_url = '';

	/**
	 * Plugin slug
	 *
	 * @var string
	 */
	protected $plugin_slug = '';

	/**
	 * Plugin Version
	 *
	 * @var string
	 */
	protected $version = '2017.0.1';

	/**
	 * Single instance of class
	 */
	protected static $_instance = null;

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function __construct() {

		$this->plugin_dir  = plugin_dir_path( __FILE__ );
		$this->plugin_url  = plugins_url( '/', __FILE__ );
		$this->plugin_slug = basename( dirname( __FILE__ ) );

		add_action( 'init', array( $this, 'init' ) );
	}

	public function init() {
		$this->includes();
	}

	public function includes() {

		require_once $this->getPluginDir() . 'libs/JCLM_Shortcode.php';
	}

	/**
	 * @return string
	 */
	public function getPluginDir() {
		return trailingslashit( $this->plugin_dir );
	}

	/**
	 * @return string
	 */
	public function getPluginUrl() {
		return trailingslashit( $this->plugin_url );
	}

	/**
	 * @return string
	 */
	public function getPluginSlug() {
		return $this->plugin_slug;
	}

	public function getPluginVersion() {
		return $this->version;
	}
}

function ALM() {
	return JC_Ajax_Load_More::instance();
}

// Global for backwards compatibility.
$GLOBALS['jc-ajax-load-more'] = ALM();