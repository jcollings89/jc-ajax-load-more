<?php

class JCLM_Shortcode {

	private $_shortcode_name = 'ajax-load-more';

	public function __construct() {
		add_shortcode( $this->_shortcode_name, array( $this, 'shortcode' ) );

		add_action( 'wp_ajax_jc_load_more', array( $this, 'load_more' ) );
		add_action( 'wp_ajax_nopriv_jc_load_more', array( $this, 'load_more' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'waypoints', ALM()->getPluginUrl() . 'assets/js/jquery.waypoints.min.js', array( 'jquery' ), ALM()->getPluginVersion(), false );
	}

	public function shortcode( $atts, $content = '' ) {

		$builder = new JCLM_Shortcode_Builder( $this->_shortcode_name, $atts );

		return $builder->output();
	}

	public function load_more() {

		$id        = esc_attr( $_GET['id'] );
		$template  = esc_attr( $_GET['template'] );
		$params    = (array) $_GET['params'];
		$paged     = intval( $_GET['paged'] );
		$param_str = '';

		foreach ( $params as $k => $v ) {
			$param_str .= ' param-' . esc_attr( $k ) . '="' . esc_attr( $v ) . '"';
		}

		echo do_shortcode( '[ajax-load-more id="' . $id . '" template="' . $template . '" ' . $param_str . ' paged="' . $paged . '" ]' );

		die();
	}
}

class JCLM_Shortcode_Builder {

	/**
	 * @var string
	 */
	private $_shortcode_id = '';

	/**
	 * @var string
	 */
	private $_shortcode_name = '';

	/**
	 * Get Shortcode Template Name
	 * @var string
	 */
	private $_template = 'default';

	/**
	 * WordPress Query to use
	 * @var WP_Query
	 */
	private $_query = null;

	/**
	 * WordPress Query Data
	 * @var array
	 */
	private $_query_data = array(
		'post_type' => 'post'
	);

	/**
	 * Display Button
	 *
	 * @var bool
	 */
	private $_button = true;

	private $_params = array();

	private $_paged = 0;

	function __construct( $name, $atts ) {

		$this->_shortcode_name = $name;

		// get params from shortcode
		$param_str = 'param-';
		if ( ! empty( $atts ) ) {
			foreach ( $atts as $k => $v ) {
				if ( strpos( $k, 'param-' ) === 0 ) {
					$this->_params[ esc_attr( substr( $k, strlen( $param_str ) ) ) ] = esc_attr( $v );
				}
			}
		}

		$atts = shortcode_atts( array(
			'id'       => '',
			'template' => $this->_template,
			'paged'    => 1,
			'button'   => 'true'
		), $atts, $this->_shortcode_name );

		if ( $atts['button'] == 'true' ) {
			$this->_button = true;
		} else {
			$this->_button = false;
		}

		$this->_shortcode_id = esc_attr( $atts['id'] );
		$this->_paged        = intval( $atts['paged'] );

		// get template name
		$this->_template = apply_filters( 'alm/template', $atts['template'], $this );

		if ( ! empty( $atts['id'] ) ) {
			$this->_template = apply_filters( sprintf( 'alm/template/%s', $atts['id'] ), $this->_template, $this );
		}

		// get wp_query
		$this->_query_data = apply_filters( 'alm/query', $this->_query_data, $this );

		if ( ! empty( $atts['id'] ) ) {
			$this->_query_data = apply_filters( sprintf( 'alm/query/%s', $atts['id'] ), $this->_query_data, $this );
		}
	}


	public function output() {

		$param_str = '';
		if ( ! empty( $this->_params ) ) {
			foreach ( $this->_params as $k => $v ) {
				if ( ! empty( $param_str ) ) {
					$param_str .= ',';
				}
				$param_str .= sprintf( '%s-%s', esc_attr( $k ), esc_attr( $v ) );
			}
		}

		$cache_key = sprintf( 'alm_%s_%s', $this->getId(), $param_str );
		$cache_key = apply_filters( 'alm/query/cache_key', $cache_key, $this );
		$cache_key = sprintf( '%s_paged_%d', $cache_key, $this->getPaged() );
		if ( true === WP_DEBUG || false === ( $output = get_transient( $cache_key ) ) ) {
			ob_start();

			if ( ! wp_doing_ajax() ) {
				echo '<div class="ajax-load-more" data-alm-id="' . $this->getId() . '">';
			}

			$this->_query_data['paged'] = intval( $this->getPaged() );
			$this->_query               = new WP_Query( $this->_query_data );
			$row_counter                = 0;

			if ( $this->_query->have_posts() ) {

				while ( $this->_query->have_posts() ) {
					$row_counter ++;
					$this->_query->the_post();

					// extract param vars
					extract( $this->_params );

					include alm_get_template( $this->getTemplate() );
				}
				wp_reset_postdata();
			}

			if ( ! wp_doing_ajax() ) {

				if(!$this->_query->have_posts()) {
					include alm_get_template( $this->getEmptyTemplate() );
				}

				// todo: this is a strange way to hide button if not needed!
				if($this->_query->max_num_pages <= 1) {
					?>
					<script>
                        (function ($) {
                            $(document).ready(function () {
                                var _alm_wrapper = $('.ajax-load-more[data-alm-id="<?php echo $this->getId(); ?>"]');
                                $('.load-more[data-alm-target="<?php echo $this->getId(); ?>"]').addClass('alm-button--disabled');
                                $(_alm_wrapper).trigger('alm-empty');
                            });
                        })(jQuery);
					</script>
					<?php
				}
			}

			if ( ! wp_doing_ajax() ) {
				echo '</div>';
				?>
				<script>
                    (function ($) {

                        var $jqajax = undefined;
                        var _disabled_btn_class = 'alm-button--disabled';
                        var _loading_btn_class = 'alm-button--loading';

                        var _alm_load_more = function () {
                            var _alm_wrapper;
                            var _self = $(this);

                            // set list target from data-alm-target=""
                            if (_self.data('alm-target') !== undefined) {
                                _alm_wrapper = $('.ajax-load-more[data-alm-id="' + _self.data('alm-target') + '"]');
                            } else {
                                _alm_wrapper = $('.ajax-load-more[data-alm-id="<?php echo $this->getId(); ?>"]');
                            }

                            var _paged = _alm_wrapper.data('paged');

                            if ($jqajax !== undefined) {
                                $jqajax.abort();
                            }

                            if (_self.data('text-default') === undefined) {
                                _self.data('text-default', _self.text());
                            }

                            _self.text(_self.data('text-loading'));
                            _self.addClass(_loading_btn_class);

                            if (_paged === undefined) {
                                _paged = <?php echo $this->getPaged() + 1; ?>;
                            }

                            $jqajax = $.get('<?php echo admin_url( 'admin-ajax.php' ); ?>', {
                                'action': 'jc_load_more',
                                'template': '<?php echo $this->getTemplate(); ?>',
                                'id': '<?php echo $this->getId(); ?>',
                                'params': <?php echo json_encode( $this->_params ); ?>,
                                'paged': _paged
                            }, function (response) {

                                _self.text(_self.data('text-default'));
                                _self.removeClass(_loading_btn_class);

                                if (response.length > 0) {

                                    _alm_wrapper.append(response);

                                    jQuery('body').trigger("contentLoaded");

                                    _paged++;
                                    _alm_wrapper.data('paged', _paged);

                                } else {
                                    Waypoint.disableAll();
                                    _self.addClass(_disabled_btn_class);
                                    $(_alm_wrapper).trigger('alm-empty');
                                }
                            });
                        };

                        $('body').on('click', '.load-more:not(.' + _disabled_btn_class + ')', _alm_load_more);

                        var _waypoints;

                        $(document).on('ready', function () {

                            _waypoints = $('.load-more').waypoint({
                                handler: function (direction) {
                                    $('.load-more').trigger('click');
                                },
                                offset: '100%',
                                continuous: false
                            });

                        });

                        $('body').on('contentLoaded', function () {
                            Waypoint.refreshAll();
                        });

                    })(jQuery);
				</script>
				<?php
				if ( $this->displayButton() ) {
					$this->output_button();
				}
			}

			$output = ob_get_clean();
			set_transient( $cache_key, $output, HOUR_IN_SECONDS );
		}

		return $output;
	}

	public function output_button() {
		?>
		<a class="load-more" data-alm-target="<?php echo $this->getId(); ?>" data-text-loading="Loading More Posts...">Load
			More Posts</a>
		<?php
	}

	/**
	 * @return string
	 */
	public function getTemplate() {
		return $this->_template;
	}

	public function getEmptyTemplate() {
		return 'empty';
	}

	public function getId() {
		return $this->_shortcode_id;
	}

	/**
	 * @param string $key Paramater Key
	 *
	 * @return string
	 */
	public function getParam( $key = '' ) {
		return isset( $this->_params[ $key ] ) ? $this->_params[ $key ] : '';
	}

	/**
	 * @return int
	 */
	public function getPaged() {
		return $this->_paged;
	}

	/**
	 * Display Load More Button?
	 *
	 * @return bool
	 */
	public function displayButton() {
		return (bool) $this->_button;
	}
}

function alm_get_template( $template_name ) {

	$located       = ALM()->getPluginDir() . sprintf( 'templates/%s.php', $template_name );
	$template_file = trailingslashit( get_stylesheet_directory() ) . sprintf( 'ajax-load-more/%s.php', $template_name );
	if ( is_file( $template_file ) ) {
		$located = $template_file;
	}

	if ( is_file( $located ) ) {
		return $located;
	}

	return false;
}

new JCLM_Shortcode();