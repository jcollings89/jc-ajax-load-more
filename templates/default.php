<article id="post-<?php the_ID(); ?>" class="post">
	<h1 class="post__title"><?php the_title(); ?></h1>
	<div class="post__excerpt">
		<?php the_excerpt(); ?>
	</div>
	<a href="<?php the_permalink(); ?>" class="post__permalink">Read More</a>
</article>