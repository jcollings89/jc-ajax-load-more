# Ajax Load More WordPress Plugin

Display a list of posts and other custom post types that more items are automatically loaded once you near the end of the current list.

__Version__: 2017.0.1  
__Author__: James Collings  
__Created__: 11/06/2017  
__Updated__: 15/06/2017   

# Features

* Customise the display by adding templates to your themes folder.
* Alter what items are fetched by using built in WordPress filters.
* Fetched Results are cached, making it super fast.

## RoadMap

* Add Interface to modify the query visually.
* Add Interface to modify the display template.

# Documentation

1. Basic Shortcode
1. Using a Custom Item Template
1. Display a list of WordPress Pages
1. Display posts in a specific category

## Basic Shortcode

When using the ajax-load-more shortcode there is only one paramater that is required which is: ```id="{{LIST_ID}}``` , this is a unique reference to the list that is being displayed and will be used later when we get to altering the query and template.

```
[ajax-load-more id="{{LIST_ID}}"]
```

Using this shortcode as it is will display a list of posts, using the plugins default template.

## Using a Custom Item Template

When you want to alter how the list item is displayed you can set what template to use in the shortcode by adding the parameter: ```template="{{TEMPLATE_NAME}}""```.
 
 For example the following shortcode will make the plugin look for a template file called ```post.php``` that is located in your theme folder in the sub-directory ```ajax-load-more```.

```
[ajax-load-more id="post-list" template="post"]
```

If you dont specify a template parameter in the shortcode it will automatically look for a template file matching the id parameter, so in the above shortcode it would also look for the file ```post-list.php```

This allows you to easily alter how items are displayed.

## Display a list of WordPress Pages

By default when using the ajax-load-more shortcode it will default to fetching a list of posts, you can easily alter what is queried by using the some WordPress filters.

For this example we will set the id to __page-list__ and you will notice that this id is used as part of the query filter name ```alm/query/page-list```.

Adding the following code to your themes functions.php file will make the shortcode display a list of pages instead of posts.

```php
<?php
/**
  * Alter the Ajax Load More page query to display posts.
  *
  * @param array $query
  * @param JCLM_Shortcode_Builder $shortcode
  *
  * @return array
  */
function alm_alter_page_query($query, $shortcode){
 
 	$query['post_type'] = 'page'; // Only fetch pages
 
 	return $query;
}
add_filter('alm/query/page-list', 'alm_alter_page_query', 10, 2);
?>
```

## Display posts in a specific category

This example will show you how to pass parameters to the query filter, custom parameters can be set in the shortcode when they are prefixed with ```param-``` as shown in the following example we have added a custom parameter called __cat__.

```
[ajax-load-more id="post-list" template="post" param-cat="5"]
```

The parameters can be used for example when we alter the query, you can see in the following example that we can fetch the custom parameter by using the getParam method and passing the custom parameter key.

```php
<?php
/**
  * Alter the Ajax Load More page query to display posts.
  *
  * @param array $query
  * @param JCLM_Shortcode_Builder $shortcode
  *
  * @return array
  */
function alm_alter_post_query($query, $shortcode){
 
 	$query['post_type'] = 'post'; // Only fetch pages
 	$query['cat'] = $shortcode->getParam('cat');
 
 	return $query;
}
add_filter('alm/query/post-list', 'alm_alter_post_query', 10, 2);
?>
```

## Use Custom Load More Button

If you want to use a custom button instead of the default one you can tell the shortcode not to output it.

Please note without a load more button on the screen it will not be able to load more items onto the list.

```
[ajax-load-more id="post-list" button="false"]
```

The only required parameters of a button is that it has:

 * Has the class __load-more__
 * Has the data tag ```data-text-loading=""``` , which has the text you want displayed when items are loading
 * Has the data tag ```data-alm-target=""```, Sets which shortcode the button belongs to.
 
Example custom button
   
```html
<a class="load-more" data-alm-target="post-list" data-text-loading="I am loading posts!">Click to load posts</a>
```

# WordPress Filters

## Template Name Filters

Filters that change the template being used

### 'alm/template'

General template name filter that is called by all shortcodes.

```php
apply_filters('alm/template', string $template_name, JCLM_Shortcode_Builder $shortcode)
```

### 'alm/template/{{LIST_ID}}'

Template name filter which is only called by the shortcode that matched the {{LIST_ID}} string.

```php
apply_filters('alm/template/{{LIST_ID}}', string $template_name, JCLM_Shortcode_Builder $shortcode))
```

## Query Filters

List of filters that change the query parameters.

### 'alm/query'

General query filter that is called by all shortcodes.

```php
apply_filters('alm/query', array $query_data, JCLM_Shortcode_Builder $shortcode)
```

### 'alm/query/{{LIST_ID}}'

Query filter which is only called by the shortcode that matched the {{LIST_ID}} string.

```php
apply_filters('alm/query/{{LIST_ID}}', array $query_data, JCLM_Shortcode_Builder $shortcode)
```

## Cache Filters

### 'alm/query/cache_key'

Alter the cache key name that for the current query
 
```php
apply_filters( 'alm/query/cache_key', string $cache_key, JCLM_Shortcode_Builder $this )
```
